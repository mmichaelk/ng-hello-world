import { Component } from '@angular/core';
import { FavoriteChangeEvent } from './favorite/favorite.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'The PLANET oF The aPES';
  post = {
    isFavorite: true
  };
  tweet = {
    body: 'Some tweet message',
    isLiked: true,
    likeCount: 9
  };

  onFavoriteChanged(ev: FavoriteChangeEvent) {
    console.log('Favorite changed: ', ev);
  }
}
