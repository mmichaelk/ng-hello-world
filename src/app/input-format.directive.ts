import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appInputFormat]'
})
export class InputFormatDirective {
  // Past date into through the Input properties
  // Also using selector of directive as an alias for one property
  @Input('appInputFormat') format: string;

  constructor(private element: ElementRef) {}

  // Subscribing to the events raised from the host DOM object
  @HostListener('blur') onBlur() {
    const value: string = this.element.nativeElement.value;
    this.element.nativeElement.value =
      this.format === 'lowercase' ? value.toLowerCase() : value.toUpperCase();
  }
}
