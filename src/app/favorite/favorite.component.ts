import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewEncapsulation
} from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css'],
  // inputs: ['isFavorite'] // another way to make input properties
  styles: [
    `
      .fa-star {
        background-color: black;
        color: yellow;
      }
    `
  ],
  encapsulation: ViewEncapsulation.Emulated
})
export class FavoriteComponent implements OnInit {
  // tslint:disable-next-line: no-input-rename
  @Input('is-favorite') isFavorite: boolean;
  // tslint:disable-next-line: no-output-rename
  @Output('change') click = new EventEmitter(); // aliasing output property

  constructor() {}

  ngOnInit() {}

  onToggle() {
    this.isFavorite = !this.isFavorite;
    this.click.emit({ nextValue: this.isFavorite });
  }
}

export interface FavoriteChangeEvent {
  nextValue: boolean;
}
