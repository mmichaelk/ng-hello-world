export interface Course {
  id: number;
  name: string;
}

export class CoursesService {
  getCourser(): Array<Course> {
    return [
      {
        id: 1,
        name: 'course-wow'
      },
      {
        id: 2,
        name: 'course-das'
      },
      {
        id: 3,
        name: 'course-kek'
      }
    ];
  }
}
