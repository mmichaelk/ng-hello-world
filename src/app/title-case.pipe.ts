import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleCase'
})
export class TitleCase implements PipeTransform {
  transform(value: string) {
    if (!value) {
      return null;
    }

    return value
      .toLowerCase()
      .split(' ')
      .map((item: string, index: number) => {
        if (!item) {
          return '';
        }

        if (!['of', 'the'].includes(item) || index === 0) {
          return item[0].toUpperCase() + item.substr(1);
        }

        return item;
      })
      .join(' ');
  }
}
