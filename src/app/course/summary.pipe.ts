import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'summary'
})
export class SummaryPipe implements PipeTransform {
  transform(value: string, length: number = 30) {
    if (!value) {
      return null;
    }

    return value.substr(0, length) + '...';
  }
}
