import { Component, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'like',
  templateUrl: './like.component.html',
  styleUrls: ['./like.component.css']
})
export class LikeComponent {
  // tslint:disable-next-line: no-input-rename
  @Input('likeCount') likeCount: number;
  // tslint:disable-next-line: no-input-rename
  @Input('isActive') isActive: boolean;

  onClick() {
    this.isActive = !this.isActive;
    this.likeCount += this.isActive ? 1 : -1;
  }
}
