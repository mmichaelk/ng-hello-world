import { Component } from '@angular/core';
import { CoursesService, Course } from './courses.service';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'courses',
  template: `
    <button class="btn btn-primary" (click)="onAddCorse()">add</button>
    <h2>{{ title + ' wow ' + getSomeValue() }}</h2>
    <ul>
      <li
        *ngFor="
          let course of courses;
          index as i;
          even as isEven;
          trackBy: trackCourses
        "
      >
        {{ course.name }} - {{ i + 1 }}<span *ngIf="isEven">🦔</span
        ><button class="btn" (click)="onRemoveCorse(course)">remove</button>
      </li>
    </ul>
    <img src="{{ imageUrl }}/cats" /> <img [src]="imageUrl" />
    <div class="propagationExample" (click)="onDivClick()">
      <button
        class="btn btn-primary"
        [class.disabled]="!isActive"
        [ngStyle]="{
          'background-color': isActive ? 'yellow' : 'red',
          color: isActive ? 'black' : 'white'
        }"
        (click)="onBtnClick($event)"
      >
        wow
      </button>
    </div>
    <!-- Event Binding  + Property binding + Custom directive-->
    <input
      [value]="name"
      (keyup)="name = $event.target.value; onKeyUp($event)"
      [appInputFormat]="'uppercase'"
    />
    <!-- Event Filtering  + Template variables -->
    <input #email (keyup.enter)="logEnterPressed(email.value)" />
    <!-- Event Binding + Two-way binding -->
    <input [(ngModel)]="lastName" (keyup.enter)="logEnterPressed()" />
    <table [style.background]="[isActive ? 'white' : 'red']">
      <tr>
        <!-- Safe Traversal Operator -->
        <td [attr.colspan]="colSpan">{{ person.assignment?.name }}</td>
      </tr>
    </table>
  `
})
export class CoursesComponent {
  name = 'Karl';
  lastName = 'Paterson';
  title = 'Some title';
  courses: Array<Course>;
  imageUrl = 'http://lorempixel.com/200/100';
  colSpan = 2;
  isActive = true;
  person = {
    assignment: null
  };

  constructor(coursesService: CoursesService) {
    this.courses = coursesService.getCourser();
  }

  trackCourses(index: number, course: Course) {
    return course ? course.id : undefined;
  }

  onAddCorse() {
    this.courses.push({
      id: this.courses.length,
      name: `course-${this.courses.length}`
    });
  }

  onRemoveCorse(course: Course) {
    const index: number = this.courses.indexOf(course);
    this.courses.splice(index, 1);
  }

  getSomeValue(): number {
    return 25;
  }

  onBtnClick($event: MouseEvent) {
    $event.stopPropagation();
    console.log('Fire onBtnClick', $event);
  }

  onDivClick() {
    console.log('Fire onDivClick');
  }

  logEnterPressed(email?) {
    console.log('Enter is pressed');
    if (email) {
      console.log(email);
    }
    console.log('lastName: ', this.lastName);
  }

  onKeyUp($event: KeyboardEvent) {
    // tslint:disable-next-line: deprecation
    if ($event.keyCode === 13) {
      this.logEnterPressed();
      console.log(this.name);
    }
  }
}
